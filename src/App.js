import LoginPage from "./LoginPage";
import Welcome from "./Welcome";
import "./styles.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

export default function App() {
  return (
    <>
      {/* This is the alias of BrowserRouter i.e. Router */}
      <Router>
        <Routes>
          <Route exact path="/" element={<LoginPage />} />
          <Route exact path="welcome" element={<Welcome />} />
        </Routes>
      </Router>
    </>
  );
}
