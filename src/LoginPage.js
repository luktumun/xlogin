import React, { useEffect, useRef, useState } from "react";

import "./LoginPage.css";
import { Link } from "react-router-dom";
import Welcome from "./Welcome";

const LoginPage = (props) => {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = React.useState("");
  const ref = useRef();
  var hide = document.getElementsByClassName("my_page");
  const handleSubmit = (e) => {
    e.preventDefault();
    //...
    if (user == "" || password == "") {
      alert("Invalid username or password");
    } else if (user == "user" || password == "password") {
      setUser(user);
      ref.current = user;
      hide[0].style.display = "none";
      const newDiv = document.createElement("div");

      const newContent = document.createTextNode(
        "Welcome,  " + ref.current + "!"
      );

      newDiv.appendChild(newContent);

      document.body.appendChild(newDiv);
    } else {
      setErrorMessage("Invalid username or password");
    }
  };

  return (
    <>
      <h1>Login Page </h1>
      <form className="my_page" onSubmit={handleSubmit}>
        <label>Username: </label>
        <input
          type="text"
          placeholder="username"
          value={user}
          onChange={(e) => setUser(e.target.value)}
        />

        <label>Password: </label>
        <input
          type="text"
          placeholder="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />

        <button type="submit">Submit</button>
        {errorMessage && <div className="error"> {errorMessage} </div>}
      </form>
    </>
  );
};

export default LoginPage;
